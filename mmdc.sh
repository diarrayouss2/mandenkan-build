#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail
echo 'info' | npx @mermaid-js/mermaid-cli@10.2.4 --input '-' --output 'out.svg'
mmdc -i ./input.mmd -o output.png
