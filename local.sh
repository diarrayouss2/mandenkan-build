#!/usr/bin/bash
UI=mandenkan-ui
rm ./ui-bundle.zip
[[ -d ../${UI}/ ]] || {
  echo theme not found
  exit 1
}
[[ -f ../${UI}/build/ui-bundle.zip ]] || {
  echo bundle not found
  pushd ../${UI} && gulp bundle && popd
}

for x in podman; do
  #type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not found.  Aborting."; exit 1; }
  echo
done

function yq {
  $(which yq) "$@"
}
export -f yq

echo Using $UI
export KROKI_SAFE_MODE=unsafe
export AUTH=gldt-kL6kwmsNi7w9yv3pqzHv
export FORCE_SHOW_EDIT_PAGE_LINK="true"
echo $AUTH
sed s/\$AUTH/"$AUTH"/g local.yml >tmp.yml

pushd ../mandenkan-docs || exit
git checkout master
git commit -a -m "update by script on $(date)" && git push origin master
popd || exit

cp -v ../${UI}/build/ui-bundle.zip .
npm run slides
npx antora --stacktrace --fetch tmp.yml
#npx antora --stacktrace tmp.yml
#	git commit -a -m "Built on $(date)" && git push origin master
#--cache-dir=./.cache  \
#rm tmp.yml
cd public && python -m http.server 8080

#git commit -a -m "Built on $(date)" && git push origin mster
#local.yml > antora.log

# podman run -it --rm \
#   --security-opt label=disable \
#   -v `pwd`:/antora:z antora/antora:latest  \
# 	--stacktrace \
# 	--ui-bundle-url=ui-bundle.zip \
# 	tmp.yml
